
using LibraryManagement.Models;

namespace LibraryManagement.Repositories;

public interface IBookRepository
{
    IQueryable<Book> GetBook();

    Book GetBook(int id);

    bool BookExists(int id);

    bool BookExists(string title);

    bool CreateBook(Book Book);

    bool UpdateBook(Book Book);

    bool DeleteBook(Book Book);

    bool Save();
}