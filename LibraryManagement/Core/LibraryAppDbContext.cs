using LibraryManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.Core;


    public class BookAppDbContext: DbContext
    {
        public DbSet<Book> Books { get; set; }

        public BookAppDbContext(DbContextOptions<BookAppDbContext> options) : base(options)
        {

        }
    }
